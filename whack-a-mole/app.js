const square = document.querySelectorAll('.square');
const mole = document.querySelectorAll('.mole');
const timeLeft = document.querySelector('#time-left');
let score = document.querySelector('#score');
const btnStartGame = document.querySelector('#btn-start-game');
const btnStopGame = document.querySelector('#btn-stop-game');

let result = 0;
let currentTime;
let countdownTimerId;
let moveMoleTimerId = null;
btnStopGame.disabled = true;
let mouseHit = false; // intended to register only 1 click per mole location
let hitPosition;

btnStartGame.addEventListener('click', () => {
    startGame();
});

btnStopGame.addEventListener('click', () => {
    endGame();
});

function endGame() {
    stopCountdown();
    stopMovingMole();
    btnStartGame.disabled = false;
    btnStopGame.disabled = true;
    score.textContent = 0;
    timeLeft.textContent = 60;
    square[hitPosition - 1].classList.remove('mole');// delete mole from the grid
}

function startGame() {
    startCountdown();
    moveMole();
    btnStartGame.disabled = true;
    btnStopGame.disabled = false;
}

function randomSquare() {
    square.forEach(className => {
        className.classList.remove('mole');
    });
    let randomPosition = square[Math.floor(Math.random() * 9)];
    while (randomPosition.id === hitPosition) {
        randomPosition = square[Math.floor(Math.random() * 9)];
    }
    randomPosition.classList.add('mole');

    // assign the id of the randomPosition to hitPosition for us to use later
    hitPosition = randomPosition.id;
}

square.forEach(id => {
    id.addEventListener('mouseup', () => {
        handleMouseHits(id);
    });
});

function handleMouseHits(id) {
    if (id.id === hitPosition && mouseHit === false) {
        mouseHit = true;
        result += 1;
        score.textContent = result;
    }
}

function stopCountdown() {
    clearInterval(countdownTimerId);
}

function startCountdown() {
    currentTime = timeLeft.textContent
    countdownTimerId = setInterval(countDown, 1000);
}

function moveMole() {
    moveMoleTimerId = setInterval(randomSquare, 1000);
}

function stopMovingMole() {
    clearInterval(moveMoleTimerId);
}

function countDown() {
    currentTime--;
    mouseHit = false;
    timeLeft.textContent = currentTime;

    if (currentTime === 0) {
        endGame();
        alert('GAME OVER! Your final score is ' + result);
    }
}

