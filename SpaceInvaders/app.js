const startBtn = document.getElementById('startBtn');
const game = document.getElementById('game');
const grid = document.querySelector('.grid');
const width = 15;

startBtn.addEventListener('mousedown', () => {
    grid.innerHTML = '';
    game.classList.remove('hidden');
    startBtn.disabled = true;
    createGrid(width * width);
    startGame();
});

// document.addEventListener('DOMContentLoaded', () => {
function startGame() {
    const squares = document.querySelectorAll('.grid div');
    const resultDisplay = document.querySelector('#result');

    // createGrid(width * width);
    let currentShooterIndex = 202;
    let currentInvaderIndex = 0;
    let alientInvadersTakenDown = [];
    let result = 0;
    let direction = 1;
    let invaderId;

    //define the alien alientInvadersTakenDown
    const alienInvaders = [
        0, 1, 2, 3, 4, 5, 6, 7, 8, 9,
        15, 16, 17, 18, 19, 20, 21, 22, 23, 24,
        30, 31, 32, 33, 34, 35, 36, 37, 38, 39
    ];

    //draw the alien alientInvadersTakenDown
    alienInvaders.forEach(invader => {
        squares[currentInvaderIndex + invader].classList.add('invader')
    });

    //draw currentShooter
    squares[currentShooterIndex].classList.add('shooter');

    function moveShooter(e) {
        squares[currentShooterIndex].classList.remove('shooter');
        switch (e.keyCode) {
            case 37:
                // squares[currentShooterIndex].classList.remove('shooter');
                if (currentShooterIndex % width !== 0) {
                    currentShooterIndex--;
                }
                break;
            case 39:
                // squares[currentShooterIndex].classList.remove('shooter');
                if (currentShooterIndex % width < width - 1) {
                    currentShooterIndex++;
                }
                break;
        }
        squares[currentShooterIndex].classList.add('shooter');
    }
    document.addEventListener('keydown', moveShooter);


    // move invaders
    function moveInvaders() {
        const leftEdge = alienInvaders[0] % width === 0;
        const rightEdge = alienInvaders[alienInvaders.length - 1] % width === width - 1;

        if ((leftEdge && direction === -1) || (rightEdge && direction === 1)) {
            direction = width;
        } else if (direction === width) {
            if (leftEdge) { direction = 1; }
            else { direction = -1; }
        }
        for (let i = 0; i <= alienInvaders.length - 1; i++) {
            squares[alienInvaders[i]].classList.remove('invader');
        }
        for (let i = 0; i <= alienInvaders.length - 1; i++) {
            alienInvaders[i] += direction;
        }
        for (let i = 0; i <= alienInvaders.length - 1; i++) {
            if (!alientInvadersTakenDown.includes(i)) {
                squares[alienInvaders[i]].classList.add('invader');
            }
        }

        //decide a game over
        if (squares[currentShooterIndex].classList.contains('invader', 'shooter')) {
            resultDisplay.textContent = 'Gave Over';
            squares[currentShooterIndex].classList.add('boom');
            clearInterval(invaderId);
            startBtn.disabled = false;
        } squares
        for (let i = 0; i <= alienInvaders.length - 1; i++) {
            if (alienInvaders[i] > (squares.length - (width - 1))) {
                resultDisplay.textContent = 'Game Over';
                clearInterval(invaderId);
                startBtn.disabled = false;
            }
        }
        // decide a win
        if (alientInvadersTakenDown.length === alienInvaders.length) {
            resultDisplay.textContent = 'You win!';
            clearInterval(invaderId);
            startBtn.disabled = false;
        }
    }

    invaderId = setInterval(moveInvaders, 500);

    //shoot at alient
    function shoot(e) {
        let laserId;
        let currentLaserIndex = currentShooterIndex;

        function moveLaser() {
            squares[currentLaserIndex].classList.remove('laser');
            currentLaserIndex -= width;
            squares[currentLaserIndex].classList.add('laser');
            if (squares[currentLaserIndex].classList.contains('invader')) {
                squares[currentLaserIndex].classList.remove('laser');
                squares[currentLaserIndex].classList.remove('invader');
                squares[currentLaserIndex].classList.add('boom');

                setTimeout(() => {
                    squares[currentLaserIndex].classList.remove('boom');
                }, 250);
                clearInterval(laserId);

                const alienTakenDown = alienInvaders.indexOf(currentLaserIndex);
                alientInvadersTakenDown.push(alienTakenDown);
                result++;
                resultDisplay.textContent = result;
            }

            if (currentLaserIndex < width) {
                clearInterval(laserId);
                setTimeout(() => squares[currentLaserIndex].classList.remove('laser'), 100);
            }
        }
        switch (e.keyCode) {
            case 32:
                laserId = setInterval(moveLaser, 100);
                break;
        }
    }
    document.addEventListener('keyup', shoot);
};

function createGrid(gridSize) {
    for (let i = 0; i < gridSize; i++) {
        const div = document.createElement('div');
        grid.appendChild(div);
    }
}
