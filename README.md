## Retro grid-based games

Technology used: HTML, CSS and JavaScript

- [Memory Game](https://gitlab.com/borodin.tim/project3_minigames/-/tree/master/memory-game)

- [Whack a mole](https://gitlab.com/borodin.tim/project3_minigames/-/tree/master/whack-a-mole)

- [Nokia3310 Snake](https://gitlab.com/borodin.tim/project3_minigames/-/tree/master/Nokia3310-Snake)

- [Space Invaders](https://gitlab.com/borodin.tim/project3_minigames/-/tree/master/SpaceInvaders) - Project on Netlify: [Space Invaders](https://space-invaders-from-outers-space.netlify.app/)
